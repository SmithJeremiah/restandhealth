﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Steeltoe.Common.HealthChecks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace JeremiahAPI.SteelToe
{
    public class DatabaseHealth : IHealthContributor
    {
        private const string ID = "DatabaseHealth";
        private string DBName = "";
        private readonly ILogger<DatabaseHealth> _logger;
        public readonly IDbConnection _connection;

        public static IHealthContributor GetMySqlHealth(IConfiguration configuration, ILogger<DatabaseHealth> logger = null)
        {
            if (configuration == null)
            {
                return new DatabaseHealth(null);
            }

            return new DatabaseHealth(null);
        }

        public DatabaseHealth(IDbConnection connection = null, ILogger<DatabaseHealth> logger = null)
        {
            _connection = connection;
            _logger = logger;
            DBName = GetDbName(connection);
        }

        public string Id { get; } = ID;

        public HealthCheckResult Health()
        {
            _logger?.LogTrace("Checking {DbConnection} health", Id);
            var result = new HealthCheckResult();
            result.Details.Add("database", DBName);
            try
            {
                _connection.Open();
                var cmd = _connection.CreateCommand();
                cmd.CommandText = "SELECT 1;";
                var qresult = cmd.ExecuteScalar();
                result.Details.Add("status", HealthStatus.UP.ToString());
                result.Status = HealthStatus.UP;
                _logger?.LogTrace("{DbConnection} up!", Id);
            }
            catch (Exception e)
            {
                _logger?.LogError("{DbConnection} down! {HealthCheckException}", Id, e.Message);
                result.Details.Add("error", e.GetType().Name + ": " + e.Message);
                result.Details.Add("status", HealthStatus.DOWN.ToString());
                result.Status = HealthStatus.DOWN;
                result.Description = $"{Id} health check failed";
            }
            finally
            {
                if(_connection!=null)
                _connection.Close();
            }

            return result;
        }

        private string GetDbName(IDbConnection connection)
        {
            if (connection == null)
                return "No Database Setup Yet";

            var result = "db";
            switch (connection.GetType().Name)
            {
                case "NpgsqlConnection":
                    result = "PostgreSQL";
                    break;
                case "SqlConnection":
                    result = "SqlServer";
                    break;
                case "MySqlConnection":
                    result = "MySQL";
                    break;
                default:
                    result = "Unknown";
                    break;
            }

            return result;
        }
    }
}