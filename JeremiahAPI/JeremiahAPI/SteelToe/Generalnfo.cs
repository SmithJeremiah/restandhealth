﻿using Steeltoe.Management.Endpoint.Info;
using System;

namespace JeremiahAPI.SteelToe
{
    public class Generalnfo : IInfoContributor
    {
        public void Contribute(IInfoBuilder builder)
        {
            // pass in the info


            //builder.WithInfo("arbitraryInfo", new { someProperty = "someValue" });

            builder.WithInfo("MACHINE NAME:", Environment.MachineName);
            builder.WithInfo("OS VERSION:", Environment.OSVersion);
            builder.WithInfo("PROCESSOR COUNT:", Environment.ProcessorCount);
            builder.WithInfo("USER NAME:", Environment.UserName);
            builder.WithInfo("VERSION:", Environment.Version.ToString());
        }
    }
}