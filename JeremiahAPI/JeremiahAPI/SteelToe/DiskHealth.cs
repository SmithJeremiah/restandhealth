﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Steeltoe.Common.HealthChecks;
using Steeltoe.Management.Endpoint.Health.Contributor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace JeremiahAPI.SteelToe
{
    public class DiskHealth : IHealthContributor
    {
        private const string ID = "DiskHealth";
        private DiskSpaceContributorOptions _options;

        public DiskHealth(DiskSpaceContributorOptions options = null)
        {
            _options = options ?? new DiskSpaceContributorOptions();
        }

        public string Id { get; } = ID;

        public HealthCheckResult Health()
        {
            HealthCheckResult result = new HealthCheckResult();

            string fullPath = Path.GetFullPath(_options.Path);
            DirectoryInfo dirInfo = new DirectoryInfo(fullPath);
            if (dirInfo.Exists)
            {
                string rootName = dirInfo.Root.Name;
                DriveInfo d = new DriveInfo(rootName);
                var freeSpace = d.TotalFreeSpace;
                if (freeSpace >= _options.Threshold)
                    result.Status = HealthStatus.UP;
                else
                    result.Status = HealthStatus.DOWN;

                result.Details.Add("total", FormatBytes(d.TotalSize));
                result.Details.Add("free", FormatBytes(freeSpace));
                result.Details.Add("threshold", FormatBytes(_options.Threshold));
                result.Details.Add("status", result.Status.ToString());
            }
            return result;
        }

        private static string FormatBytes(long bytes)
        {
            string[] Suffix = { "B", "KB", "MB", "GB", "TB" };
            int i;
            try
            {
                double dblSByte = bytes;
                for (i = 0; i < Suffix.Length && bytes >= 1024; i++, bytes /= 1024)
                {
                    dblSByte = bytes / 1024.0;
                }
                return $"{dblSByte:0.##} {Suffix[i]}";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }            
        }

    }
}