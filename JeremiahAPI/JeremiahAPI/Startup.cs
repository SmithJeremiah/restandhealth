﻿using JeremiahAPI.Models;
using JeremiahAPI.SteelToe;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Steeltoe.Common.HealthChecks;
using Steeltoe.Management.Endpoint.Health;
using Steeltoe.Management.Endpoint.Info;
using Steeltoe.Management.Endpoint.Metrics;
using Steeltoe.Management.Endpoint.Trace;
using Steeltoe.Management.Exporter.Metrics;

namespace JeremiahAPI
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        { 
            // slash health
            services.AddSingleton<IHealthContributor, DiskHealth>();
            services.AddSingleton<IHealthContributor, DatabaseHealth>();
            services.AddHealthActuator(Configuration);
            // slash info
            services.AddSingleton<IInfoContributor, Generalnfo>();
            services.AddInfoActuator(Configuration);
            // slash metrics 
            services.AddMetricsActuator(Configuration);
            // slash trace
            services.AddTraceActuator(Configuration);

            services.AddDbContext<TodoContext>(opt => opt.UseInMemoryDatabase("TodoList"));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseStaticFiles();
            app.UseHealthActuator();//health
            app.UseInfoActuator();//info                                  
            app.UseMetricsActuator();//metrics
            app.UseTraceActuator();//trace

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
      
            app.UseMvc();
        }
    }
}
